import functions

def sym_gauss_int_sqr (xb, c, n=10):
    h = float(2*xb)/n
    result = 0.5*functions.gauss(-xb,1,0, c) + 0.5*functions.gauss(xb,1,0, c)
    for i in range(1, n):
        result += functions.gauss(-xb+ i*h,1,0, c)
    result *= h
    return result**2
